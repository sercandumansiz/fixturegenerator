﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixtureGenarator.Models
{
    public class MatchWeek
    {
        public int Week { get; set; }
        public List<Fixture> Fixtures { get; set; }
    }
}
