﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixtureGenarator.Enums
{
    public static class Indices
    {
        public const double Fr = 0.147058824;
        public const double Sa = 0.352941176;
        public const double Su = 0.352941176;
        public const double Mo = 0.147058824;

    }
}
