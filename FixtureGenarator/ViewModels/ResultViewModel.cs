﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixtureGenarator.ViewModels
{
    public class ResultViewModel
    {
        public string HomeTeamName { get; set; }
        public string AwayTeamName { get; set; }
        public string Week { get; set; }
        public DateTime Date { get; set; }
        public string Day { get; set; }
    }
}
