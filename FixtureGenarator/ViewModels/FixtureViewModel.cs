﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixtureGenarator.ViewModels
{
    public class FixtureViewModel
    {
        public string HomeTeamName { get; set; }
        public string AwayTeamName { get; set; }
        public string WeekName { get; set; }
    }
}
