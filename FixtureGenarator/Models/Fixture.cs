﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FixtureGenarator.Models
{
    public class Fixture
    {
        public int Id  { get; set; }
        public Guid HomeTeamId { get; set; }
        public Guid AwayTeamId { get; set; }
        public int Week { get; set; }

        [NotMapped]
        public string Day { get; set; }

        [NotMapped]
        public string DayTag { get; set; }
        
    }
}