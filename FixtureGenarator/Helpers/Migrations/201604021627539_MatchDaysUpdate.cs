namespace FixtureGenarator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MatchDaysUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MatchDays",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HomeTeamId = c.Guid(nullable: false),
                        AwayTameId = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Week = c.Int(nullable: false),
                        Day = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TeamDayCounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TeamId = c.Guid(nullable: false),
                        ThursdayMatchCount = c.Int(nullable: false),
                        FridayMatchCount = c.Int(nullable: false),
                        SaturdayMatchCount = c.Int(nullable: false),
                        SundayMatchCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TeamDayCounts");
            DropTable("dbo.MatchDays");
        }
    }
}
