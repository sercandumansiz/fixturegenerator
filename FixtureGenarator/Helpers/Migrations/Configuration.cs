using System.Collections.Generic;
using FixtureGenarator.Models;

namespace FixtureGenarator.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FixtureGenarator.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(FixtureGenarator.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            IList<Team> teams = new List<Team>();

            teams.Add(new Team() { Id = new Guid(), Name = "Akhisar Belediyespor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Antalyaspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "�stanbul Ba�ak�ehir" });
            teams.Add(new Team() { Id = new Guid(), Name = "Be�ikta�" });
            teams.Add(new Team() { Id = new Guid(), Name = "Bursaspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "�aykur Rizespor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Eski�ehirspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Fenerbah�e" });
            teams.Add(new Team() { Id = new Guid(), Name = "Galatasaray" });
            teams.Add(new Team() { Id = new Guid(), Name = "Gaziantepspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Gen�lerbirli�i" });
            teams.Add(new Team() { Id = new Guid(), Name = "Kas�mpa�a" });
            teams.Add(new Team() { Id = new Guid(), Name = "Kayserispor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Mersin �dman Yurdu" });
            teams.Add(new Team() { Id = new Guid(), Name = "Osmanl�spor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Sivasspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Torku Konyaspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Trabzonspor" });

            foreach (var team in teams)
            {
                context.Teams.Add(team);
            }
           
            
        }
    }
}
