﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixtureGenarator.Models
{
    public class MatchDays
    {
        public int Id { get; set; }
        public Guid HomeTeamId { get; set; }
        public Guid AwayTameId { get; set; }
        public DateTime Date { get; set; }
        public int Week { get; set; }
        public string Day { get; set; }

    }
}
