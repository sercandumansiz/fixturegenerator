﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FixtureGenarator.Startup))]
namespace FixtureGenarator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
