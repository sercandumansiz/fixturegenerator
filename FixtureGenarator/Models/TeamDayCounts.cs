﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixtureGenarator.Models
{
    public class TeamDayCounts
    {
        public int Id { get; set; }
        public Guid TeamId { get; set; }
        public int FridayMatchCount { get; set; }
        public int SaturdayMatchCount { get; set; }
        public int SundayMatchCount { get; set; }
        public int MondayMatchCount { get; set; }
    }
}
