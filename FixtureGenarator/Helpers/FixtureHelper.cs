﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FixtureGenarator.Enums;
using FixtureGenarator.Models;

namespace FixtureGenarator.Helpers
{

    public class FixtureHelper
    {
        private readonly static Random Random = new Random();
        private readonly List<Team> _teams;
        private readonly int _matchCount;
        private ApplicationDbContext db = new ApplicationDbContext();

        public FixtureHelper(List<Team> teams, int matchCount)
        {
            _teams = teams;
            _matchCount = matchCount;
        }
        public FixtureHelper()
        {

        }

        public List<MatchWeek> GenerateFixtures()
        {
            var matchWeeks = new List<MatchWeek>();
            int numberOfGamesWeeks = (_teams.Count * _matchCount) - _matchCount;

            for (int i = 1; i <= numberOfGamesWeeks; i++)
            {
                matchWeeks.Add(GenerateFixturesForMatchDay(i));
            }

            return matchWeeks;
        }

        private MatchWeek GenerateFixturesForMatchDay(int week)
        {
            var fixtures = new List<Fixture>();

            var teams = new List<Team>(_teams);
            while (teams.Count > 0)
            {
                Team homeTeam = SelectTeam(teams);
                Team awayTeam = SelectTeam(teams);
                fixtures.Add(new Fixture()
                {
                    AwayTeamId = awayTeam.Id,
                    HomeTeamId = homeTeam.Id,
                    Week = week
                });
            }

            var matchWeek = new MatchWeek()
            {
                Fixtures = fixtures,
                Week = week
            };
            return matchWeek;
        }

        public Team SelectTeam(List<Team> teams)
        {
            int teamIndex = Random.Next(0, teams.Count);
            Team team = teams[teamIndex];
            teams.RemoveAt(teamIndex);
            return team;
        }


        public void GenerateMatchDaysForFirstWeek(Fixture fixture, DateTime startDate)
        {
            var matchDays = new List<MatchDays>();
            var endDate = startDate.AddDays(fixture.Week * 7);


            foreach (DateTime day in EachDay(startDate, endDate))
            {
                var matchDay = new MatchDays();

                var addedMatches =
                    db.MatchDays.FirstOrDefault(
                        r =>
                            r.AwayTameId == fixture.AwayTeamId && r.HomeTeamId == fixture.HomeTeamId &&
                            r.Week == fixture.Week);


                if (addedMatches == null)
                {
                    if (day.DayOfWeek.ToString() == "Friday" && db.MatchDays.Count(x => x.Week == fixture.Week && x.Day == "Friday") == 0)
                    {
                        matchDay.AwayTameId = fixture.AwayTeamId;
                        matchDay.HomeTeamId = fixture.HomeTeamId;
                        matchDay.Date = day;
                        matchDay.Day = day.DayOfWeek.ToString();
                        matchDay.Week = fixture.Week;
                        db.MatchDays.Add(matchDay);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.FridayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                FridayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.FridayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                FridayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }



                        db.SaveChanges();
                    }

                    if (day.DayOfWeek.ToString() == "Saturday" && (db.MatchDays.Count(x => x.Week == fixture.Week && x.Day == "Saturday") < 3))
                    {
                        matchDay.AwayTameId = fixture.AwayTeamId;
                        matchDay.HomeTeamId = fixture.HomeTeamId;
                        matchDay.Date = day;
                        matchDay.Day = day.DayOfWeek.ToString();
                        matchDay.Week = fixture.Week;
                        db.MatchDays.Add(matchDay);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.SaturdayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                SaturdayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.SaturdayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                SaturdayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }

                        db.SaveChanges();
                    }

                    if (day.DayOfWeek.ToString() == "Sunday" && (db.MatchDays.Count(x => x.Week == fixture.Week && x.Day == "Sunday") < 4))
                    {
                        matchDay.AwayTameId = fixture.AwayTeamId;
                        matchDay.HomeTeamId = fixture.HomeTeamId;
                        matchDay.Date = day;
                        matchDay.Day = day.DayOfWeek.ToString();
                        matchDay.Week = fixture.Week;
                        db.MatchDays.Add(matchDay);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.SundayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                SundayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.SundayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                SundayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }

                        db.SaveChanges();
                    }

                    if (day.DayOfWeek.ToString() == "Monday" && db.MatchDays.Count(x => x.Week == fixture.Week && x.Day == "Monday") == 0)
                    {
                        matchDay.AwayTameId = fixture.AwayTeamId;
                        matchDay.HomeTeamId = fixture.HomeTeamId;
                        matchDay.Date = day;
                        matchDay.Day = day.DayOfWeek.ToString();
                        matchDay.Week = fixture.Week;
                        db.MatchDays.Add(matchDay);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.MondayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                MondayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.MondayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                MondayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }

                        db.SaveChanges();
                    }
                }
                else
                {
                    break;
                }

            }


        }

        public void GenerateMatchDaysForOtherWeeks(Fixture fixture, DateTime startDate)
        {
              var matchDays = new List<MatchDays>();
            var endDate = startDate.AddDays(fixture.Week * 7);


            foreach (DateTime day in EachDay(startDate, endDate))
            {
                var matchDay = new MatchDays();

                var addedMatches =
                    db.MatchDays.FirstOrDefault(
                        r =>
                            r.AwayTameId == fixture.AwayTeamId && r.HomeTeamId == fixture.HomeTeamId &&
                            r.Week == fixture.Week);


                if (addedMatches == null)
                {
                    if (day.DayOfWeek.ToString() == "Friday" && db.MatchDays.Count(x => x.Week == fixture.Week && x.Day == "Friday") == 0)
                    {
                        matchDay.AwayTameId = fixture.AwayTeamId;
                        matchDay.HomeTeamId = fixture.HomeTeamId;
                        matchDay.Date = day;
                        matchDay.Day = day.DayOfWeek.ToString();
                        matchDay.Week = fixture.Week;
                        db.MatchDays.Add(matchDay);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.FridayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                FridayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.FridayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                FridayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }



                        db.SaveChanges();
                    }

                    if (day.DayOfWeek.ToString() == "Saturday" && (db.MatchDays.Count(x => x.Week == fixture.Week && x.Day == "Saturday") < 3))
                    {
                        matchDay.AwayTameId = fixture.AwayTeamId;
                        matchDay.HomeTeamId = fixture.HomeTeamId;
                        matchDay.Date = day;
                        matchDay.Day = day.DayOfWeek.ToString();
                        matchDay.Week = fixture.Week;
                        db.MatchDays.Add(matchDay);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.SaturdayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                SaturdayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.SaturdayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                SaturdayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }

                        db.SaveChanges();
                    }

                    if (day.DayOfWeek.ToString() == "Sunday" && (db.MatchDays.Count(x => x.Week == fixture.Week && x.Day == "Sunday") < 4))
                    {
                        matchDay.AwayTameId = fixture.AwayTeamId;
                        matchDay.HomeTeamId = fixture.HomeTeamId;
                        matchDay.Date = day;
                        matchDay.Day = day.DayOfWeek.ToString();
                        matchDay.Week = fixture.Week;
                        db.MatchDays.Add(matchDay);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.SundayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                SundayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.SundayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                SundayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }

                        db.SaveChanges();
                    }

                    if (day.DayOfWeek.ToString() == "Monday" && db.MatchDays.Count(x => x.Week == fixture.Week && x.Day == "Monday") == 0)
                    {
                        matchDay.AwayTameId = fixture.AwayTeamId;
                        matchDay.HomeTeamId = fixture.HomeTeamId;
                        matchDay.Date = day;
                        matchDay.Day = day.DayOfWeek.ToString();
                        matchDay.Week = fixture.Week;
                        db.MatchDays.Add(matchDay);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.MondayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                MondayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.MondayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                MondayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }

                        db.SaveChanges();
                    }
                }
                else
                {
                    break;
                }

            }
        }

        public Dictionary<Fixture, double> CalculateTheLowestPoint(int week, List<Fixture> fixtureList, Fixture fixture, Guid homeTeamId, Guid awayTeamId, DateTime date)
        {
            fixture.Day = date.DayOfWeek.ToString();
            var result = new Dictionary<Fixture, double>();
            if (date.DayOfWeek.ToString() == "Friday")
            {
                double total = 0;
                var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId).FridayMatchCount;
                var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId).FridayMatchCount;

                if (homeTeamId == fixture.HomeTeamId)
                {
                    double resultHomeTeam = Math.Pow(1 + homeTeamDayCount - (week * Indices.Fr), 2);
                    total += resultHomeTeam;
                }

                if (awayTeamId == fixture.AwayTeamId)
                {
                    double resultAwayTeam = Math.Pow(1 + awayTeamDayCount - (week * Indices.Fr), 2);
                    total += resultAwayTeam;
                }

                foreach (var match in fixtureList.Where(x => x.AwayTeamId != awayTeamId && x.HomeTeamId != homeTeamId))
                {
                    var otherAwayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == match.AwayTeamId).FridayMatchCount;
                    var otherHomeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == match.HomeTeamId).FridayMatchCount;
                    double otherAwayTeam = Math.Pow(0 + otherAwayTeamDayCount - (week * Indices.Fr), 2);
                    double otherHomeTeam = Math.Pow(0 + otherHomeTeamDayCount - (week * Indices.Fr), 2);
                    total += otherAwayTeam + otherHomeTeam;
                }


                var sapma = Math.Sqrt(total);
                
                result.Add(fixture,sapma);
                
            }

            if (date.DayOfWeek.ToString() == "Saturday")
            {
                double total = 0;
                var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId).SaturdayMatchCount;
                var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId).SaturdayMatchCount;

                if (homeTeamId == fixture.HomeTeamId)
                {
                    double resultHomeTeam = Math.Pow(1 + homeTeamDayCount - (week * Indices.Sa), 2);
                    total += resultHomeTeam;
                }

                if (awayTeamId == fixture.AwayTeamId)
                {
                    double resultAwayTeam = Math.Pow(1 + awayTeamDayCount - (week * Indices.Sa), 2);
                    total += resultAwayTeam;
                }

                foreach (var match in fixtureList.Where(x => x.AwayTeamId != awayTeamId && x.HomeTeamId != homeTeamId))
                {
                    var otherAwayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == match.AwayTeamId).SaturdayMatchCount;
                    var otherHomeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == match.HomeTeamId).SaturdayMatchCount;
                    double otherAwayTeam = Math.Pow(0 + otherAwayTeamDayCount - (week * Indices.Sa), 2);
                    double otherHomeTeam = Math.Pow(0 + otherHomeTeamDayCount - (week * Indices.Sa), 2);
                    total += otherAwayTeam + otherHomeTeam;
                }


                var sapma = Math.Sqrt(total);
                result.Add(fixture, sapma);
            }



            if (date.DayOfWeek.ToString() == "Sunday")
            {
                double total = 0;
                var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId).SundayMatchCount;
                var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId).SundayMatchCount;

                if (homeTeamId == fixture.HomeTeamId)
                {
                    double resultHomeTeam = Math.Pow(1 + homeTeamDayCount - (week * Indices.Su), 2);
                    total += resultHomeTeam;
                }

                if (awayTeamId == fixture.AwayTeamId)
                {
                    double resultAwayTeam = Math.Pow(1 + awayTeamDayCount - (week * Indices.Su), 2);
                    total += resultAwayTeam;
                }

                foreach (var match in fixtureList.Where(x => x.AwayTeamId != awayTeamId && x.HomeTeamId != homeTeamId))
                {
                    var otherAwayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == match.AwayTeamId).SundayMatchCount;
                    var otherHomeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == match.HomeTeamId).SundayMatchCount;
                    double otherAwayTeam = Math.Pow(0 + otherAwayTeamDayCount - (week * Indices.Su), 2);
                    double otherHomeTeam = Math.Pow(0 + otherHomeTeamDayCount - (week * Indices.Su), 2);
                    total += otherHomeTeam + otherAwayTeam;
                }


                var sapma = Math.Sqrt(total);
                result.Add(fixture, sapma);
            }



            if (date.DayOfWeek.ToString() == "Monday")
            {
                double total = 0;
                var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId).MondayMatchCount;
                var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId).MondayMatchCount;

                if (homeTeamId == fixture.HomeTeamId)
                {
                    double resultHomeTeam = Math.Pow(1 + homeTeamDayCount - (week * Indices.Mo), 2);
                    total += resultHomeTeam;
                }

                if (awayTeamId == fixture.AwayTeamId)
                {
                    double resultAwayTeam = Math.Pow(1 + awayTeamDayCount - (week * Indices.Mo), 2);
                    total += resultAwayTeam;
                }

                foreach (var match in fixtureList.Where(x => x.AwayTeamId != awayTeamId && x.HomeTeamId != homeTeamId))
                {
                    var otherAwayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == match.AwayTeamId).MondayMatchCount;
                    var otherHomeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == match.HomeTeamId).MondayMatchCount;
                    double otherAwayTeam = Math.Pow(0 + otherAwayTeamDayCount - (week * Indices.Mo), 2);
                    double otherHomeTeam = Math.Pow(0 + otherHomeTeamDayCount - (week * Indices.Mo), 2);
                    total += otherAwayTeam + otherHomeTeam;
                }


                    var sapma = Math.Sqrt(total);
                    result.Add(fixture, sapma);
            }

            return result;



        }

        public void TakeTheLowestWeekPoint(int week)
        {

            var weekList = db.Fixtures.Where(x => x.Week == week).ToList();

            DateTime startDate = new DateTime(2014, 10, 01);
            var endDate = startDate.AddDays(week * 7);
         

            List<Dictionary<Fixture, double>> fridayDeviations = new List<Dictionary<Fixture, double>>(); 
            List<Dictionary<Fixture, double>> saturdayDeviations = new List<Dictionary<Fixture, double>>();
            List<Dictionary<Fixture, double>> sundayDeviations = new List<Dictionary<Fixture, double>>();
            List<Dictionary<Fixture, double>> mondayDeviations = new List<Dictionary<Fixture, double>>();

            List<List<Dictionary<Fixture, Double>>> allList = new List<List<Dictionary<Fixture, double>>>();


            foreach (DateTime day in EachDay(endDate.AddDays(-7), endDate))
            {
                foreach (var fixture in weekList.ToList())
                {
                    var test = CalculateTheLowestPoint(week, weekList , fixture, fixture.HomeTeamId, fixture.AwayTeamId, day);
                    if (day.DayOfWeek.ToString() == "Friday")
                    {
                        var matchDeviation = db.MatchDeviations.FirstOrDefault(x => x.FixtureId == fixture.Id);
                        if (matchDeviation != null)
                        {
                            matchDeviation.FridayValue = test.Values.FirstOrDefault();
                            db.MatchDeviations.AddOrUpdate(matchDeviation);
                            db.SaveChanges();
                        }
                        else
                        {
                            var matchDeviations = new MatchDeviations
                            {
                                FixtureId = fixture.Id,
                                FridayValue = test.Values.FirstOrDefault(),
                            }; 
                            db.MatchDeviations.AddOrUpdate(matchDeviations);
                            db.SaveChanges();

                        }
                    }
                    if (day.DayOfWeek.ToString() == "Saturday")
                    {
                        var matchDeviation = db.MatchDeviations.FirstOrDefault(x => x.FixtureId == fixture.Id);
                        if (matchDeviation != null)
                        {
                            matchDeviation.SaturdayValue = test.Values.FirstOrDefault();
                            db.MatchDeviations.AddOrUpdate(matchDeviation);
                            db.SaveChanges();
                        }
                        else
                        {
                            var matchDeviations = new MatchDeviations
                            {
                                FixtureId = fixture.Id,
                                SaturdayValue = test.Values.FirstOrDefault(),
                            };
                            db.MatchDeviations.AddOrUpdate(matchDeviations);
                            db.SaveChanges();

                        }
                    }
                    if (day.DayOfWeek.ToString() == "Sunday")
                    {
                        var matchDeviation = db.MatchDeviations.FirstOrDefault(x => x.FixtureId == fixture.Id);
                        if (matchDeviation != null)
                        {
                            matchDeviation.SundayValue = test.Values.FirstOrDefault();
                            db.MatchDeviations.AddOrUpdate(matchDeviation);
                            db.SaveChanges();
                        }
                        else
                        {
                            var matchDeviations = new MatchDeviations
                            {
                                FixtureId = fixture.Id,
                                SundayValue = test.Values.FirstOrDefault(),
                            };
                            db.MatchDeviations.AddOrUpdate(matchDeviations);
                            db.SaveChanges();

                        }
                    }
                    if (day.DayOfWeek.ToString() == "Monday")
                    {
                        var matchDeviation = db.MatchDeviations.FirstOrDefault(x => x.FixtureId == fixture.Id);
                        if (matchDeviation != null)
                        {
                            matchDeviation.MondayValue = test.Values.FirstOrDefault();
                            db.MatchDeviations.AddOrUpdate(matchDeviation);
                            db.SaveChanges();
                        }
                        else
                        {
                            var matchDeviations = new MatchDeviations
                            {
                                FixtureId = fixture.Id,
                                MondayValue = test.Values.FirstOrDefault(),
                            };
                            db.MatchDeviations.AddOrUpdate(matchDeviations);
                            db.SaveChanges();

                        }
                    }
                }
            }


            var weekPoints = db.MatchDeviations.ToList().Where(r => weekList.Any(z => z.Id == r.FixtureId)).ToList();
            List<Tuple<MatchDeviations, double?>> fridayValues =  new List<Tuple<MatchDeviations, double?>>();
            List<Tuple<MatchDeviations, double?>> saturdayValues = new List<Tuple<MatchDeviations, double?>>();
            List<Tuple<MatchDeviations, double?>> sundayValues = new List<Tuple<MatchDeviations, double?>>();
            List<Tuple<MatchDeviations, double?>>  mondayValues = new List<Tuple<MatchDeviations, double?>>();
            var points = weekPoints.SelectMany(x => new List<double?> { x.FridayValue.Value , x.SaturdayValue.Value , x.SundayValue.Value , x.MondayValue.Value }).ToList();
            for (int i = 0; i < 35; i++)
            {
                var pointsMin = points.Min();
                points.Remove(points.Min());

                foreach (var weekPoint in weekPoints.ToList())
                {
                    if (String.Format("{0:0.####}", weekPoint.FridayValue) == String.Format("{0:0.####}", points.Min()) && fridayValues.Count < 1)
                    {
                        fridayValues.Add(new Tuple<MatchDeviations, double?>(weekPoint, weekPoint.FridayValue));
                        weekPoints.Remove(weekPoint);
                        break;
                    }
                    if (String.Format("{0:0.####}", weekPoint.SaturdayValue) == String.Format("{0:0.####}", points.Min()) && saturdayValues.Count < 3)
                    {
                        saturdayValues.Add(new Tuple<MatchDeviations, double?>(weekPoint, weekPoint.SaturdayValue));
                        weekPoints.Remove(weekPoint);
                        break;
                    }
                    if (String.Format("{0:0.####}", weekPoint.SundayValue) == String.Format("{0:0.####}", points.Min()) && sundayValues.Count < 4)
                       
                    {
                        sundayValues.Add(new Tuple<MatchDeviations, double?>(weekPoint, weekPoint.SundayValue));
                        weekPoints.Remove(weekPoint);
                        break;
                    }
                    if (String.Format("{0:0.####}", weekPoint.MondayValue) == String.Format("{0:0.####}", points.Min()) && mondayValues.Count < 1)
                    {
                        mondayValues.Add(new Tuple<MatchDeviations, double?>(weekPoint, weekPoint.MondayValue));
                        weekPoints.Remove(weekPoint);
                        break;
                    }
                }

           
            }

                foreach (var item in fridayValues)
                {

                    foreach (DateTime day in EachDay(endDate.AddDays(-7), endDate).Where(x => x.DayOfWeek.ToString() == "Friday"))
                    {
                        var fixture = db.Fixtures.FirstOrDefault(x => x.Id == item.Item1.FixtureId);
                        var matchDays = new MatchDays
                        {
                            HomeTeamId = fixture.HomeTeamId,
                            AwayTameId = fixture.AwayTeamId,
                            Date = day,
                            Week = week,
                            Day = day.DayOfWeek.ToString()
                        };
                        db.MatchDays.Add(matchDays);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.FridayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                FridayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.FridayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                FridayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }
                        db.SaveChanges();
                    }
                }

                foreach (var item in saturdayValues)
                {
                    foreach (DateTime day in EachDay(endDate.AddDays(-7), endDate).Where(x => x.DayOfWeek.ToString() == "Saturday"))
                    {
                        var fixture = db.Fixtures.FirstOrDefault(x => x.Id == item.Item1.FixtureId);
                        var matchDays = new MatchDays
                        {
                            HomeTeamId = fixture.HomeTeamId,
                            AwayTameId = fixture.AwayTeamId,
                            Date = day,
                            Week = week,
                            Day = day.DayOfWeek.ToString()
                        };
                        db.MatchDays.Add(matchDays);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.SaturdayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                SaturdayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.SaturdayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                SaturdayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }
                        db.SaveChanges();
                    }
                }

                foreach (var item in sundayValues)
                {
                    foreach (DateTime day in EachDay(endDate.AddDays(-7), endDate).Where(x => x.DayOfWeek.ToString() == "Sunday"))
                    {
                        var fixture = db.Fixtures.FirstOrDefault(x => x.Id == item.Item1.FixtureId);
                        var matchDays = new MatchDays
                        {
                            HomeTeamId = fixture.HomeTeamId,
                            AwayTameId = fixture.AwayTeamId,
                            Date = day,
                            Week = week,
                            Day = day.DayOfWeek.ToString()
                        };
                        db.MatchDays.Add(matchDays);

                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.SundayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                SundayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.SundayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                SundayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }
                        db.SaveChanges();
                    }
                }

                foreach (var item in mondayValues)
                {
                    foreach (DateTime day in EachDay(endDate.AddDays(-7), endDate).Where(x => x.DayOfWeek.ToString() == "Monday"))
                    {
                        var fixture = db.Fixtures.FirstOrDefault(x => x.Id == item.Item1.FixtureId);
                        var matchDays = new MatchDays
                        {
                            HomeTeamId = fixture.HomeTeamId,
                            AwayTameId = fixture.AwayTeamId,
                            Date = day,
                            Week = week,
                            Day = day.DayOfWeek.ToString()
                        };
                        db.MatchDays.Add(matchDays);


                        var homeTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.HomeTeamId);
                        var awayTeamDayCount = db.TeamDayCounts.FirstOrDefault(x => x.TeamId == fixture.AwayTeamId);

                        if (homeTeamDayCount != null)
                        {
                            homeTeamDayCount.MondayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(homeTeamDayCount);
                        }
                        else
                        {
                            var newHomeDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.HomeTeamId,
                                MondayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newHomeDayCount);
                        }

                        if (awayTeamDayCount != null)
                        {
                            awayTeamDayCount.MondayMatchCount += 1;
                            db.TeamDayCounts.AddOrUpdate(awayTeamDayCount);
                        }
                        else
                        {
                            var newAwayDayCount = new TeamDayCounts()
                            {
                                TeamId = fixture.AwayTeamId,
                                MondayMatchCount = 1
                            };
                            db.TeamDayCounts.AddOrUpdate(newAwayDayCount);
                        }
                        db.SaveChanges();
                    }
                }
        }

        
            //var minimumWeekPointsByDay = new Dictionary<double?,Fixture>();
            //minimumWeekPointsByDay.Add(weekPoints.Min(x => x.FridayValue),);
            //minimumWeekPointsByDay.Add(weekPoints.Min(x => x.SaturdayValue));
            //minimumWeekPointsByDay.Add(weekPoints.Min(x => x.SundayValue));
            //minimumWeekPointsByDay.Add(weekPoints.Min(x => x.MondayValue));

            




         
            
        


        
       
       


        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date < thru.Date; day = day.AddDays(1))
                yield return day;
        }
    }

}
