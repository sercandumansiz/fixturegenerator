﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FixtureGenarator.Helpers;
using FixtureGenarator.Models;
using FixtureGenarator.ViewModels;

namespace FixtureGenarator.Controllers
{
    public class FixtureController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        //FIXTURE OLUSTURUR
        public void GenerateFixture()
        {
            var count = 0;
            var fixtures = db.Fixtures.ToList();
            var teams = db.Teams.ToList();

            var fixtureHelper = new FixtureHelper(teams,2);
            var result = fixtureHelper.GenerateFixtures();

            foreach (var match in result)
            {
                foreach (var fixture in match.Fixtures)
                {
                    count++;
                    try
                    {
                        db.Fixtures.Add(new Fixture()
                        {
                            Id = count,
                            AwayTeamId = fixture.AwayTeamId,
                            HomeTeamId = fixture.HomeTeamId,
                            Week = match.Week 
                        });
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        
                        throw;
                    }
                    
                }
            }
        }


        //İLK HAFTAYI HESAPLAR
        public void FixtureToDays()
        {
            var fixtureHelper = new FixtureHelper();

            var fixtureList = db.Fixtures.ToList().Where(x=> x.Week== 1);

            foreach (var fixture in fixtureList)
            {
                fixtureHelper.GenerateMatchDaysForFirstWeek(fixture, new DateTime(2014, 10, 01));
            }
        }

        //FIXTURU GOSTERIR
        public ActionResult ShowFixture()
        {
            var fixtureList = db.Fixtures.ToList();
            if (!(fixtureList.Count > 0))
            {
                GenerateFixture();
            }

            fixtureList = db.Fixtures.ToList();
            var viewModelList = new List<FixtureViewModel>();
            foreach (var item in fixtureList)
            {
                var awayTeam = db.Teams.FirstOrDefault(x => x.Id == item.AwayTeamId);
                var homeTeam = db.Teams.FirstOrDefault(x => x.Id == item.HomeTeamId);

                viewModelList.Add(new FixtureViewModel()
                {
                    AwayTeamName = awayTeam.Name,
                    HomeTeamName = homeTeam.Name,
                    WeekName = item.Week + ". Week"
                });

            }
            return View(viewModelList);
        }

        public ActionResult FixtureToDayFor34Week()
        {
            FixtureHelper helper = new FixtureHelper();
            for (int i = 2; i < 35; i++)
            {
                helper.TakeTheLowestWeekPoint(i);
            }
            return RedirectToAction("ShowFixtureGeneratorResult");
        }

        public ActionResult ShowFixtureGeneratorResult()
        {
            var matchDays = db.MatchDays.ToList();
            var resultViewModel = new List<ResultViewModel>();
            foreach (var match in matchDays)
            {
                var fixture = db.Fixtures.Where(x => x.HomeTeamId == match.HomeTeamId && x.AwayTeamId == match.AwayTameId);
                var result = new ResultViewModel();
                result.AwayTeamName = db.Teams.FirstOrDefault(x => x.Id == match.AwayTameId).Name;
                result.HomeTeamName = db.Teams.FirstOrDefault(x => x.Id == match.HomeTeamId).Name;
                result.Day = match.Date.DayOfWeek.ToString();
                result.Date = match.Date;
                result.Week = match.Week.ToString() + "Week";
                resultViewModel.Add(result);
            }

            return View(resultViewModel);
        }
    }
}