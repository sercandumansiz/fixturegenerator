namespace FixtureGenarator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TeamDayCounts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeamDayCounts", "MondayMatchCount", c => c.Int(nullable: false));
            DropColumn("dbo.TeamDayCounts", "ThursdayMatchCount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TeamDayCounts", "ThursdayMatchCount", c => c.Int(nullable: false));
            DropColumn("dbo.TeamDayCounts", "MondayMatchCount");
        }
    }
}
