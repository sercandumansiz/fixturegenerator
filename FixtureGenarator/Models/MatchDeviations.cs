﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixtureGenarator.Models
{
    public class MatchDeviations
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FixtureId { get; set; }
        public double? FridayValue { get; set; }
        public double? SaturdayValue { get; set; }
        public double? SundayValue { get; set; }
        public double? MondayValue { get; set; }
    }
}
