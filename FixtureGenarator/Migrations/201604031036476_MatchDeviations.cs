namespace FixtureGenarator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MatchDeviations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MatchDeviations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FixtureId = c.Int(nullable: false),
                        FridayValue = c.Double(),
                        SaturdayValue = c.Double(),
                        SundayValue = c.Double(),
                        MondayValue = c.Double(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MatchDeviations");
        }
    }
}
