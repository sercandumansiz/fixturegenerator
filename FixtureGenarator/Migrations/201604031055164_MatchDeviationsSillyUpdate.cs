namespace FixtureGenarator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MatchDeviationsSillyUpdate : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MatchDeviations");
            AlterColumn("dbo.MatchDeviations", "FixtureId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.MatchDeviations", "FixtureId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.MatchDeviations");
            AlterColumn("dbo.MatchDeviations", "FixtureId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.MatchDeviations", "FixtureId");
        }
    }
}
