﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FixtureGenarator.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
           Database.SetInitializer<ApplicationDbContext>(new DbInitializer());
        }

        public DbSet<Team> Teams { get; set; }
        public DbSet<Fixture> Fixtures { get; set; }
        public DbSet<MatchDays> MatchDays { get; set; }
        public DbSet<TeamDayCounts> TeamDayCounts { get; set; }
        public DbSet<MatchDeviations> MatchDeviations { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
        }
    }

    public class DbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            IList<Team> teams = new List<Team>();

            teams.Add(new Team() { Id = new Guid(), Name = "Akhisar Belediyespor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Antalyaspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "İstanbul Başakşehir" });
            teams.Add(new Team() { Id = new Guid(), Name = "Beşiktaş" });
            teams.Add(new Team() { Id = new Guid(), Name = "Bursaspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Çaykur Rizespor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Eskişehirspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Fenerbahçe" });
            teams.Add(new Team() { Id = new Guid(), Name = "Galatasaray" });
            teams.Add(new Team() { Id = new Guid(), Name = "Gaziantepspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Gençlerbirliği" });
            teams.Add(new Team() { Id = new Guid(), Name = "Kasımpaşa" });
            teams.Add(new Team() { Id = new Guid(), Name = "Kayserispor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Mersin İdman Yurdu" });
            teams.Add(new Team() { Id = new Guid(), Name = "Osmanlıspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Sivasspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Torku Konyaspor" });
            teams.Add(new Team() { Id = new Guid(), Name = "Trabzonspor" });

            foreach (var team in teams)
            {
                context.Teams.Add(team);
            }
          
            base.Seed(context);
        }
    }
}