namespace FixtureGenarator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MatchDeviationsUpdate : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MatchDeviations");
            AlterColumn("dbo.MatchDeviations", "FixtureId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.MatchDeviations", "FixtureId");
            DropColumn("dbo.MatchDeviations", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MatchDeviations", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.MatchDeviations");
            AlterColumn("dbo.MatchDeviations", "FixtureId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.MatchDeviations", "Id");
        }
    }
}
